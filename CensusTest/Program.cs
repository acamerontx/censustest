﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CensusTest
{
    class Program
    {
        public static string sourceFilename = @"c:\TestCensusCSV.txt";
        public static string outputFilename = @"c:\TestCensusOutput.txt";
        public static int minYear = 1900;
        public static int maxYear = 2000;
        public static int csvBirthYearIndex = 2; //index in csv source file for birth year
        public static int csvDeathYearIndex = 3; //index in csv source file for death year
        public static int maxRandomPeople = 10000;
        private static int maxNumberPeople = 0;

       

        static void Main(string[] args)
        {
            if (!File.Exists(sourceFilename))
            {
                Random rand = new Random();
               // File.Create(sourceFilename);
                CreateRandomPeopleDataset(rand.Next(0, maxRandomPeople));
            }

            List <int> maxYears = ProcessCSV();
            using (StreamWriter writer =  File.CreateText(outputFilename))
            {
                StringBuilder message = new StringBuilder();
                message.Append("There was a maximum population of " + maxNumberPeople.ToString() + " in year");
                if (maxYears.Count() > 1)
                {
                    message.Append("s: ");
                }
                else
                {
                    message.Append(": ");
                }

                foreach (int year in maxYears)
                {
                    message.AppendLine(year.ToString());
                }
                writer.WriteLine(message.ToString());
                    
            }


        }

        public static void CreateRandomPeopleDataset(int numberPeople)
        {
            using (StreamWriter fileWriter = File.CreateText(sourceFilename))
            {
                int birthYear = 0;
                int deathYear = 0;
                Random rand = new Random();
                for (int i = 0; i < numberPeople; i++)
                {
                    birthYear = rand.Next(minYear,maxYear + 1); //to include max year
                    deathYear = rand.Next(birthYear, maxYear + 1);
                    fileWriter.WriteLine("John,Smith," + birthYear.ToString() + ',' + deathYear.ToString());
                }
            }
            
        }

        //this will process a file in csv format of the style firstname, lastname, birthyear, deathyear
        public static List<int> ProcessCSV()
        {
            int [] years = new int[maxYear - minYear + 1];// to include end years

            using (StreamReader fileReader = new StreamReader(File.OpenRead(sourceFilename)))
            {
                string[] personArray;
                String personLine;
                int birthYear;
                int deathYear;
                bool success;


                while (!fileReader.EndOfStream)
                {
                    personLine = fileReader.ReadLine();
                    personArray = personLine.Split(',');
                    success = int.TryParse(personArray[csvBirthYearIndex], out birthYear);
                    if (success)
                    {
                        success = int.TryParse(personArray[csvDeathYearIndex], out deathYear);
                        if (success &&
                            birthYear <= deathYear &&
                            birthYear >= minYear && birthYear <= maxYear &&
                            deathYear >= minYear && deathYear <= maxYear)
                        {
                            int birthIndex = birthYear - minYear;
                            int deathIndex = deathYear - minYear;
                            for (int i = birthIndex; i <= deathIndex; i++)
                            {
                                years[i]++;
                            }
                        }
                    }
                }
            }

            List<int> highestPopulation = new List<int>();
            int currentPopulationCount = 0;
             maxNumberPeople = 0;
            for (int i = 0; i < years.Length; i++)
            {
                currentPopulationCount = years[i];
                if (currentPopulationCount == maxNumberPeople)
                {
                    highestPopulation.Add(i + minYear);
                }
                else if (currentPopulationCount > maxNumberPeople)
                {
                    maxNumberPeople = currentPopulationCount;
                    highestPopulation = new List<int>();
                    highestPopulation.Add(i + minYear);
                }
            }
            return highestPopulation;
           

        }
    }
}
